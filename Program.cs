﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Lesson28_PartII_HW
{
    class Program
    {

        static void Main(string[] args)
        {
            //creating Employee list
            List<Employee> employees = new List<Employee>();

            // connect to the Data Base
            using (SQLiteConnection con =
                new SQLiteConnection("Data Source = D:\\sqlite\\employee.db; Version = 3;"))
            {

                // open the connection
                con.Open();

                // create a query (suign the connection)
                using (SQLiteCommand cmd = new SQLiteCommand("SELECT * From Employee ", con))
                {

                    // execut4e the query into the reader
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {

                        // use the reader to read all of the results of the query
                        while (reader.Read())
                        {
                            //adding values from SQLite into List of Employees
                            Employee emp = new Employee(Convert.ToInt32(reader.GetValue(0)), reader.GetValue(1).ToString(), reader.GetValue(2).ToString(), Convert.ToInt32(reader.GetValue(3)));
                            employees.Add(emp);                            
                        }
                    }

                }

                Console.WriteLine();
            }
            employees.ForEach(e => Console.WriteLine(e));
        }
    }
}