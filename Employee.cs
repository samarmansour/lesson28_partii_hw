﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson28_PartII_HW
{
    class Employee
    {
        public int ID { get; private set; }
        public string NAME { get; private set; }
        public string CITY { get; private set; }
        public int SALARY { get; private set; }

        public Employee(int id, string name, string city, int salary)
        {
            ID = id;
            NAME = name;
            CITY = city;
            SALARY = salary;
        }

        public override string ToString()
        {
            return $"ID: {ID}  Name: {NAME}  City: {CITY}  Salary: {SALARY}";
        }
    }
}
